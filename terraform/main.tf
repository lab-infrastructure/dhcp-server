terraform {
  backend "http" {
  }
}

module "dhcp" {
  source = "gitlab.com/lab-infrastructure/xoa-static-instance/local"
  version = "0.0.7"

  vm_names = [
    "dhcp1"
    ]
  vm_ipv4_addresses = [
    "10.0.30.4"
    ]
  vm_ipv4_addresses_cidr = "24"
  vm_name_description = "DHCP Server"
  default_gateway = "10.0.30.1"
  realm = "core.dylanlab.xyz"
  
  username_admin = var.username_admin
  public_key_admin = var.public_key_admin
  username_ansible = var.username_ansible
  public_key_ansible = var.public_key_ansible
  xen_pool_name = "xcp-ng-pool-01"
  xen_template_name = "ubuntu-focal-20.04-cloudimg-20220124"
  xen_storage_name = "iscsi-vm-store"
  xen_network_name = "core.dylanlab.xyz"

  update_dns = true
  dns_server_primary = "10.0.30.2"
  dns_server_secondary = "10.0.30.3"
  dns_server_primary_key_name = "dylanlab.xyz."
  dns_server_primary_key_algorithm = "hmac-sha256"
  dns_server_primary_key_secret = "QZnULB75ySdwR1CNHx3Bjx6CXJKQBa/jcjTfPceoBXU="

  vm_wait_for_ip = true

}

output "hostnames" {
  value = module.dhcp.instance_hostnames
}

output "ipv4" {
  value = module.dhcp.instance_ipv4_address_first
}


resource "local_file" "ansible_inventory" {
  filename = "../files/generated/ansible/inventory/hosts.yaml"
  content = yamlencode({
    "all" : {
      "vars" : {
        "ansible_user": "ansible",
        "ansible_ssh_private_key_file": "~/.ssh/ansible_id_rsa",
        "ansible_python_interpreter": "/usr/bin/python3",
        "ansible_ssh_common_args": "-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null",
      },
      "children" : {
        "dhcp" : {
          "hosts" : zipmap(module.dhcp.instance_hostnames, [for e in module.dhcp.instance_hostnames : {}])
        }
      }
  } })
}

resource "null_resource" "ansible" {
  provisioner "local-exec" {
    command = "ansible-playbook -i ../files/generated/ansible/inventory/hosts.yaml ../ansible/main.yaml"
  }
  depends_on = [
    module.dhcp,
    local_file.ansible_inventory
  ]
}
